from my_logger.my_logger import Logger
import requests
from typing import Dict, Any, Optional


class NitradoServer:
    """
    Attributes
    ----------
    api_token : str
    server_id : str
    logger : Logger

    Methods
    -------
    get_headers() -> Dict[str, str]:
        Returns the headers for the API requests.
    get_players_datas() -> None:
        Fetches and prints the data of the players connected to the server.
    get_server_status() -> None:
        Fetches and prints the status of the server.
    get_server_resources() -> None:
        Fetches and prints the resource usage of the server.
    get_game_datas() -> None:
        Fetches and prints the game data of the server.
    """

    def __init__(self) -> None:
        """Initializes NitradoServer with the given API token and server ID."""
        self.api_token: str = "api_token"
        self.server_id: str = "server_id"
        self.logger: Logger = Logger('nitrado_logger')

    def get_headers(self) -> Dict[str, str]:
        """Returns the headers for the API requests."""
        return {
            'Authorization': f'Bearer {self.api_token}',
        }

    def get_players_datas(self) -> None:
        """
        Fetches and prints the data of the players connected to the server.

        It makes a GET request to the Nitrado API and retrieves the data of the players.
        If the request is successful, it prints the name and playtime of each player.
        If the request fails, it logs the error and prints a message.
        """
        try:
            response: requests.Response = requests.get(f'https://api.nitrado.net/services/{self.server_id}/gameservers',
                                                       headers=self.get_headers())
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.logger.error(f"Request error: {e}")
            print('Unable to retrieve player information')
            return

        data: Dict[str, Any] = response.json()
        players: Optional[Dict[str, Any]] = data['data']['gameserver']['game']['players']
        if players is not None:
            for player in players:
                self.logger.info(f"Player {player['name']} is connected for {player['playtime']} minutes")
                print(f"{player['name']} is connected for {player['playtime']} minutes")
        else:
            self.logger.warning("No players connected")

    def get_server_status(self) -> None:
        """
        Fetches and prints the status of the server.

        It makes a GET request to the Nitrado API and retrieves the status of the server.
        If the request is successful, it prints the status of the server.
        If the request fails, it logs the error and prints a message.
        """
        try:
            response: requests.Response = requests.get(f'https://api.nitrado.net/services/{self.server_id}',
                                                       headers=self.get_headers())
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.logger.error(f"Request error: {e}")
            print('Unable to retrieve server status')
            return

        data: Dict[str, Any] = response.json()
        status: str = data['data']['status']
        self.logger.info(f"Server status: {status}")
        print(f"Server status: {status}")

    def get_server_resources(self) -> None:
        """
        Fetches and prints the resource usage of the server.

        It makes a GET request to the Nitrado API and retrieves the resource usage of the server.
        If the request is successful, it prints the CPU usage, memory usage, and bandwidth usage of the server.
        If the request fails, it logs the error and prints a message.
        """
        try:
            response: requests.Response = requests.get(f'https://api.nitrado.net/services/{self.server_id}/usage',
                                                       headers=self.get_headers())
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.logger.error(f"Request error: {e}")
            print('Unable to retrieve server resources')
            return

        data: Dict[str, Any] = response.json()
        cpu_usage: int = data['data']['cpu']
        memory_usage: int = data['data']['memory']
        bandwidth_usage: int = data['data']['bandwidth']
        self.logger.info(f"CPU usage: {cpu_usage}%")
        self.logger.info(f"Memory usage: {memory_usage}%")
        self.logger.info(f"Bandwidth usage: {bandwidth_usage} Mbps")
        print(f"CPU usage: {cpu_usage}%")
        print(f"Memory usage: {memory_usage}%")
        print(f"Bandwidth usage: {bandwidth_usage} Mbps")

    def get_game_datas(self) -> None:
        """
        Fetches and prints the game data of the server.

        It makes a GET request to the Nitrado API and retrieves the game data of the server.
        If the request is successful, it prints the name and version of the game running on the server.
        If the request fails, it logs the error and prints a message.
        """
        try:
            response: requests.Response = requests.get(f'https://api.nitrado.net/services/{self.server_id}/gameservers',
                                                       headers=self.get_headers())
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.logger.error(f"Request error: {e}")
            print('Unable to retrieve game info')
            return

        data: Dict[str, Any] = response.json()
        game_name: str = data['data']['gameserver']['game']['name']
        game_version: str = data['data']['gameserver']['game']['version']
        self.logger.info(f"Game: {game_name}")
        self.logger.info(f"Version: {game_version}")
        print(f"Game: {game_name}")
        print(f"Version: {game_version}")


nitrado_server: NitradoServer = NitradoServer()
nitrado_server.get_players_datas()
nitrado_server.get_server_status()
nitrado_server.get_server_resources()
nitrado_server.get_game_datas()
